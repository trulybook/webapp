const PROXY_CONFIG = {
    '/': {
        target: 'http://34.90.38.47',
        secure: false,
        changeOrigin: true,
        logLevel: 'debug'
    }
}

module.exports = PROXY_CONFIG;
