export const CONSTANTS = {
    BANNER: {
        MOTTO: 'La tua storia nasce qui'
    },
    HOME: {
        THE_STORIES: 'Le storie',
        IN_EVIDENCE: 'In evidenza',
        VIDEO: 'Video',
        AUDIO: 'Audio'
    },
    CARD_HOME: {
        AUTHOR_OF: 'di',
        READ: 'Leggi il libro',
        SHOW_VIDEO: 'Presentazione video',
        HEAR_AUDIO: 'Presentazione audio'
    },
    CARD_EXTENDED: {
        READ: 'Leggi',
        SHOW: 'Guarda',
        HEAR: 'Ascolta'
    },
    CARD_EXTENDED_SMALL: {
        READ: 'Leggi',
        SHOW: 'Guarda',
        HEAR: 'Ascolta'
    },
    SIDEBAR: {
        ABOUT: 'About',
        SOCIAL: 'Social'
    },
    FOOTER: {
        RESUME: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor augue nulla, non scelerisque nisl pellentesque ut. Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        TAG: {
            CATEGORY: 'TAG',
            BOOK: 'Book',
            PUBLISHING: 'Publishing',
            CREATIVE_STORIES: 'Creative stories',
            STORY_TELLING: 'Story telling',
            JOBS: 'Jobs'
        },
        ABOUT: {
            CATEGORY: 'ABOUT',
            ABOUT_US: 'About us',
            HOW_IT_WORKS: 'How it works',
            JOBS: 'Jobs'
        },
        SOCIAL: {
            CATEGORY: 'SOCIAL',
            NEWSLETTER: 'Newsletter',
            EVENTS: 'Events'
        },
        COPYRIGHT: 'Copyright 2021 © TRULY BOOK'
    },
    LABEL: {
        CLOSE: 'Chiudi'
    }
};
