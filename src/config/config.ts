import {environment} from '../environments/environment';

const api = environment.url;
const bucket = environment.bucketStorage;

export let config = {
    api: {
        getBooks: api + '/books/getBooks',
        getContentVideo: api + '/books/getContentVideo',
        getContentAudio: api + '/books/getContentAudio',
        getBooksByAuthor: api + '/books/getBooksByAuthor',
        getContentReadByBook: api + '/books/getContentReadByBook',
        getContentVideoByBook: api + '/books/getContentVideoByBook',
        getContentAudioByBook: api + '/books/getContentAudioByBook'
    },
    pathContent: {
        avatar: bucket + '/avatars',
        book: bucket + '/books'
    }
};
