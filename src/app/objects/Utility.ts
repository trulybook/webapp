import {IMenu} from '../interfaces/IMenu';

export class Utility {

    constructor() {
    }

    static getNavigationMenu(): Array<IMenu> {
        return [
            {label: 'LIBRI', url: '/libri'},
            {label: 'SCOPRI', url: '/scopri'}
        ];
    }

    static getAuthMenu(): Array<IMenu> {
        return [
            {label: 'LOG IN', url: '/login'}
        ];
    }

    static getAbout(): Array<IMenu> {
        return [
            {label: 'Bio', url: 'http://www.google.it/'},
            {label: 'Books', url: 'http://www.google.it/'},
            {label: 'Jobs', url: 'http://www.google.it/'},
            {label: 'View our catalogue', url: 'http://www.google.it/'}
        ];
    }

    static getSocial(): Array<IMenu> {
        return [
            {label: 'Facebook', url: 'http://www.google.it/'},
            {label: 'Twitter', url: 'http://www.google.it/'},
            {label: 'Instagram', url: 'http://www.google.it/'}
        ];
    }

}
