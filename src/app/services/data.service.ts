import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {IBook} from '../interfaces/IBook';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private booksSubject = new BehaviorSubject<any>({});
    booksObservable = this.booksSubject.asObservable();

    constructor() {
    }

    setBooks(books: Array<IBook>) {
        this.booksSubject.next(books);
    }

    getBooks(): Observable<IBook[]> {
        return this.booksSubject;
    }

}
