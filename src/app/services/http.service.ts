import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IBook} from '../interfaces/IBook';
import {Observable} from 'rxjs';
import {config} from '../../config/config';
import {IVideo} from '../interfaces/IVideo';
import {IAudio} from '../interfaces/IAudio';
import {IAuthorDetails} from '../interfaces/IAuthorDetails';
import {IContents} from '../interfaces/IContents';
import {Cacheable} from 'angular-cacheable';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(
        private http: HttpClient
    ) {
    }

    getBooks(): Observable<IBook[]> {
        const url = config.api.getBooks;
        return this.http.get<IBook[]>(url);
    }

    getContentVideo(): Observable<IVideo[]> {
        const url = config.api.getContentVideo;
        return this.http.get<IVideo[]>(url);
    }

    getContentAudio(): Observable<IAudio[]> {
        const url = config.api.getContentAudio;
        return this.http.get<IAudio[]>(url);
    }

    @Cacheable({
        key: (args: any[]) => {
            return 'author-books-' + args[0];
        }
    })
    getBooksByAuthor(author): Observable<IAuthorDetails> {
        const url = config.api.getBooksByAuthor + '/' + author;
        return this.http.get<IAuthorDetails>(url);
    }

    getContentReadByBook(book): Observable<IContents> {
        const url = config.api.getContentReadByBook + '/' + book;
        return this.http.get<IContents>(url);
    }

    getContentVideoByBook(book): Observable<IContents> {
        const url = config.api.getContentVideoByBook + '/' + book;
        return this.http.get<IContents>(url);
    }

    getContentAudioByBook(book): Observable<IContents> {
        const url = config.api.getContentAudioByBook + '/' + book;
        return this.http.get<IContents>(url);
    }

}
