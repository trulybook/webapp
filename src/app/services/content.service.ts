import {Injectable} from '@angular/core';
import {config} from '../../config/config';
import {IBook} from '../interfaces/IBook';
import {ILibro} from '../interfaces/ILibro';
import {IVideo} from '../interfaces/IVideo';
import {IAudio} from '../interfaces/IAudio';

@Injectable({
    providedIn: 'root'
})
export class ContentService {

    config = config;

    constructor() {
    }

    getCoverBookUrl(book: IBook): string {
        return config.pathContent.book + '/' + book.id_book + '/' + book.img;
    }

    getContentLibroUrl(content: ILibro): string {
        return config.pathContent.book + '/' + content.id_book + '/libro/' + content.content_book_filename;
    }

    getContentVideoUrl(content: IVideo): string {
        return config.pathContent.book + '/' + content.id_book + '/video/' + content.content_video_filename;
    }

    getContentAudioUrl(content: IAudio): string {
        return config.pathContent.book + '/' + content.id_book + '/audio/' + content.content_audio_filename;
    }

}
