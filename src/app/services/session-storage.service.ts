import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SessionStorageService {

    constructor() {
    }

    setStorage(key: string, value: string): void {
        sessionStorage.setItem(key, value);
    }

    getStorage(key: string): string {
        return sessionStorage.getItem(key);
    }

}
