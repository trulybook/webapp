import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {IBook} from '../../interfaces/IBook';
import {HttpService} from '../../services/http.service';
import {SessionStorageService} from '../../services/session-storage.service';
import {IVideo} from '../../interfaces/IVideo';
import {IAudio} from '../../interfaces/IAudio';
import {CONSTANTS} from '../../../config/constants';
import {IConstants} from '../../interfaces/IConstants';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ContentService} from '../../services/content.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

    CONSTANTS: IConstants = CONSTANTS;
    books$: IBook[][] = [];
    video$: IVideo[][] = [];
    audio$: IAudio[][] = [];
    selectedTypeContent: string;
    selectedContent: IVideo | IAudio;
    subscriptions: Array<Subscription> = [];
    @ViewChild('modal', {read: TemplateRef}) modal: TemplateRef<any>;

    constructor(
        public httpService: HttpService,
        public sessionStorageService: SessionStorageService,
        public router: Router,
        private modalService: NgbModal,
        public contentService: ContentService
    ) {
    }

    ngOnInit(): void {
        window.scroll(0, 0);
        this.getBooks();
        this.getContentVideo();
        this.getContentAudio();
    }

    getBooks(): void {
        if (!this.sessionStorageService.getStorage('books')) {
            this.subscriptions.push(this.httpService.getBooks().subscribe((data: Array<IBook>) => {
                this.books$.push(data.slice(0, 3));
                this.books$.push(data.slice(3));
                this.sessionStorageService.setStorage('books', JSON.stringify(this.books$));
            }));
        } else {
            this.books$ = JSON.parse(this.sessionStorageService.getStorage('books'));
        }
    }

    getContentVideo(): void {
        if (!this.sessionStorageService.getStorage('video')) {
            this.subscriptions.push(this.httpService.getContentVideo().subscribe((data: Array<IVideo>) => {
                this.video$.push(data.slice(0, 3));
                this.video$.push(data.slice(3));
                this.sessionStorageService.setStorage('video', JSON.stringify(this.video$));
            }));
        } else {
            this.video$ = JSON.parse(this.sessionStorageService.getStorage('video'));
        }
    }

    getContentAudio(): void {
        if (!this.sessionStorageService.getStorage('audio')) {
            this.subscriptions.push(this.httpService.getContentAudio().subscribe((data: Array<IAudio>) => {
                this.audio$.push(data.slice(0, 3));
                this.audio$.push(data.slice(3));
                this.sessionStorageService.setStorage('audio', JSON.stringify(this.audio$));
            }));
        } else {
            this.audio$ = JSON.parse(this.sessionStorageService.getStorage('audio'));
        }
    }

    openContentPage(typeContent: string, book: IBook): void {
        this.router.navigate(['author/' + book.id_author + '/' + typeContent + '/' + book.id_book]).then();
    }

    openModal($event, content: any): void {
        this.selectedTypeContent = $event;
        this.selectedContent = content;
        this.modalService.open(this.modal, {centered: true}).result.then(() => {
        }, () => {
        });
    }

    ngOnDestroy(): void {
        this.subscriptions.map((subscription: Subscription) => {
            subscription.unsubscribe();
        });
    }

}
