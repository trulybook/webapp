import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home/home.component';
import {ComponentsModule} from '../components/components.module';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import {DirectivesModule} from '../directives/directives.module';
import {AuthorComponent} from './author/author.component';
import {AuthorBooksListComponent} from './author/author-books-list/author-books-list.component';
import {ContentBookListComponent} from './author/content-book-list/content-book-list.component';
import {PdfJsViewerModule} from 'ng2-pdfjs-viewer';

@NgModule({
    declarations: [
        HomeComponent,
        AuthorComponent,
        AuthorBooksListComponent,
        ContentBookListComponent
    ],
    imports: [
        CommonModule,
        ComponentsModule,
        RouterModule,
        FormsModule,
        DirectivesModule,
        NgbCarouselModule,
        PdfJsViewerModule
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class PagesModule {
}
