import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpService} from '../../services/http.service';
import {Utility} from '../../objects/Utility';
import {IMenu} from '../../interfaces/IMenu';
import {IAuthorDetails} from '../../interfaces/IAuthorDetails';
import {DataService} from '../../services/data.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-author',
    templateUrl: './author.component.html',
    styleUrls: ['./author.component.scss']
})
export class AuthorComponent implements OnInit, OnDestroy {

    id: number;
    about$: Array<IMenu> = Utility.getAbout();
    social$: Array<IMenu> = Utility.getSocial();
    authorDetails: IAuthorDetails;
    subscriptions: Array<Subscription> = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private httpService: HttpService,
        private dataService: DataService
    ) {
    }

    ngOnInit(): void {
        window.scroll(0, 0);
        this.id = Number(this.route.snapshot.paramMap.get('id'));
        this.getContents(this.id);
    }

    getContents(id: number): void {
        this.subscriptions.push(this.httpService.getBooksByAuthor(id).subscribe((data: IAuthorDetails) => {
            this.authorDetails = data;
            this.dataService.setBooks(this.authorDetails.books);
        }, (error) => {
            this.router.navigate(['/home']).then();
        }));
    }

    ngOnDestroy(): void {
        this.subscriptions.map((subscription: Subscription) => {
            subscription.unsubscribe();
        });
    }

}
