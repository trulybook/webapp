import {Component, OnInit} from '@angular/core';
import {IBook} from '../../../interfaces/IBook';
import {DataService} from '../../../services/data.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-author-books-list',
    templateUrl: './author-books-list.component.html',
    styleUrls: ['./author-books-list.component.scss']
})
export class AuthorBooksListComponent implements OnInit {

    books$: Array<IBook>;

    constructor(
        private dataService: DataService,
        private router: Router
    ) {
        this.dataService.getBooks().subscribe((data: IBook[]) => {
            this.books$ = data;
        });
    }

    ngOnInit(): void {
    }

    openContentPage(typeContent: string, book: IBook): void {
        this.router.navigate([this.router.url + '/' + typeContent + '/' + book.id_book]).then();
    }

}
