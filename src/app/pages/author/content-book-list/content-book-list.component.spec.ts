import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ContentBookListComponent} from './content-book-list.component';

describe('ContentBookListComponent', () => {
    let component: ContentBookListComponent;
    let fixture: ComponentFixture<ContentBookListComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ContentBookListComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ContentBookListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
