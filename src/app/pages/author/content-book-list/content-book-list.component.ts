import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpService} from '../../../services/http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {IContents} from '../../../interfaces/IContents';
import {IVideo} from '../../../interfaces/IVideo';
import {IAudio} from '../../../interfaces/IAudio';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ContentService} from '../../../services/content.service';
import {IConstants} from '../../../interfaces/IConstants';
import {CONSTANTS} from '../../../../config/constants';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-content-book-list',
    templateUrl: './content-book-list.component.html',
    styleUrls: ['./content-book-list.component.scss']
})
export class ContentBookListComponent implements OnInit, OnDestroy {

    CONSTANTS: IConstants = CONSTANTS;
    type: string;
    contents: IContents;
    selectedTypeContent: string;
    selectedContent: IVideo | IAudio;
    subscriptions: Array<Subscription> = [];
    @ViewChild('modal', {read: TemplateRef}) modal: TemplateRef<any>;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private httpService: HttpService,
        private modalService: NgbModal,
        public contentService: ContentService
    ) {
    }

    ngOnInit(): void {
        window.scroll(0, 0);
        this.type = this.route.snapshot.paramMap.get('type');
        const id = Number(this.route.snapshot.paramMap.get('id'));
        this.getContentsByType(id);
    }

    getContentsByType(id: number): void {
        switch (this.type) {
            case 'libro':
                this.subscriptions.push(this.httpService.getContentReadByBook(id).subscribe((data: IContents) => {
                    this.contents = data;
                }, (error) => {
                    this.router.navigate(['/home']).then();
                }));
                break;
            case 'video':
                this.subscriptions.push(this.httpService.getContentVideoByBook(id).subscribe((data: IContents) => {
                    this.contents = data;
                }, (error) => {
                    this.router.navigate(['/home']).then();
                }));
                break;
            case 'audio':
                this.subscriptions.push(this.httpService.getContentAudioByBook(id).subscribe((data: IContents) => {
                    this.contents = data;
                }, (error) => {
                    this.router.navigate(['/home']).then();
                }));
                break;
        }
    }

    openModal($event, content: any): void {
        this.selectedTypeContent = $event;
        this.selectedContent = content;
        this.modalService.open(this.modal, {centered: true}).result.then(() => {
        }, () => {
        });
    }

    ngOnDestroy(): void {
        this.subscriptions.map((subscription: Subscription) => {
            subscription.unsubscribe();
        });
    }

}
