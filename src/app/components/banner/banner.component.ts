import {Component, OnInit} from '@angular/core';
import {CONSTANTS} from "../../../config/constants";
import {IConstants} from "../../interfaces/IConstants";

@Component({
    selector: 'app-banner',
    templateUrl: './banner.component.html',
    styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

    CONSTANTS: IConstants = CONSTANTS;

    constructor() {
    }

    ngOnInit(): void {
    }

}
