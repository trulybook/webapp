import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {IBook} from '../../interfaces/IBook';
import {IAudio} from '../../interfaces/IAudio';
import {IVideo} from '../../interfaces/IVideo';
import {CONSTANTS} from '../../../config/constants';
import {IConstants} from '../../interfaces/IConstants';

@Component({
    selector: 'app-card-extended',
    templateUrl: './card-extended.component.html',
    styleUrls: ['./card-extended.component.scss']
})
export class CardExtendedComponent implements OnInit {

    CONSTANTS: IConstants = CONSTANTS;
    @Input() book: IBook | IAudio | IVideo;
    @Input() type: string;
    @Input() showButtons: boolean;
    @Output() contentTypeSelected = new EventEmitter<string>();

    constructor() {
    }

    ngOnInit(): void {
    }

    selectTypeContent(typeContent: string): void {
        this.contentTypeSelected.emit(typeContent);
    }

}
