import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IBook} from '../../interfaces/IBook';
import {IVideo} from '../../interfaces/IVideo';
import {IAudio} from '../../interfaces/IAudio';
import {CONSTANTS} from '../../../config/constants';
import {IConstants} from '../../interfaces/IConstants';
import {ContentService} from '../../services/content.service';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

    CONSTANTS: IConstants = CONSTANTS;
    @Input() book: IBook | IVideo | IAudio;
    @Input() type: string;
    @Output() contentTypeSelected = new EventEmitter<string>();

    constructor(
        public contentService: ContentService
    ) {
    }

    ngOnInit(): void {
    }

    selectTypeContent(typeContent: string): void {
        this.contentTypeSelected.emit(typeContent);
    }

}
