import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IBook} from '../../interfaces/IBook';
import {CONSTANTS} from '../../../config/constants';
import {IConstants} from '../../interfaces/IConstants';

@Component({
    selector: 'app-card-extended-small',
    templateUrl: './card-extended-small.component.html',
    styleUrls: ['./card-extended-small.component.scss']
})
export class CardExtendedSmallComponent implements OnInit {

    CONSTANTS: IConstants = CONSTANTS;
    @Input() book: IBook;
    @Input() type: string;
    @Output() contentTypeSelected = new EventEmitter<string>();

    constructor() {
    }

    ngOnInit(): void {
    }

    selectTypeContent(typeContent: string): void {
        this.contentTypeSelected.emit(typeContent);
    }

}
