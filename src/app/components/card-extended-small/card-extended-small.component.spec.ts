import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CardExtendedSmallComponent} from './card-extended-small.component';

describe('CardExtendedSmallComponent', () => {
    let component: CardExtendedSmallComponent;
    let fixture: ComponentFixture<CardExtendedSmallComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [CardExtendedSmallComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CardExtendedSmallComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
