import {Component, OnInit} from '@angular/core';
import {CONSTANTS} from "../../../config/constants";
import {IConstants} from "../../interfaces/IConstants";

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    CONSTANTS: IConstants = CONSTANTS;

    constructor() {
    }

    ngOnInit(): void {
    }

}
