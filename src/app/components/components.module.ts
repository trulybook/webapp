import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {HeaderComponent} from './header/header.component';
import {CardComponent} from './card/card.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {FooterComponent} from './footer/footer.component';
import {DirectivesModule} from '../directives/directives.module';
import {SidebarComponent} from './sidebar/sidebar.component';
import {CardExtendedComponent} from './card-extended/card-extended.component';
import {CardExtendedSmallComponent} from './card-extended-small/card-extended-small.component';
import {BannerComponent} from './banner/banner.component';
import {BannerAuthorComponent} from './banner-author/banner-author.component';
import {RelatedBooksComponent} from './related-books/related-books.component';

@NgModule({
    declarations: [
        HeaderComponent,
        CardComponent,
        FooterComponent,
        SidebarComponent,
        CardExtendedComponent,
        CardExtendedSmallComponent,
        BannerComponent,
        BannerAuthorComponent,
        RelatedBooksComponent
    ],
    exports: [
        HeaderComponent,
        CardComponent,
        FooterComponent,
        SidebarComponent,
        CardExtendedComponent,
        CardExtendedSmallComponent,
        BannerComponent,
        BannerAuthorComponent,
        RelatedBooksComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        FontAwesomeModule,
        NgbDropdownModule,
        DirectivesModule
    ]
})
export class ComponentsModule {
}
