import {Component, Input, OnInit} from '@angular/core';
import {IAuthor} from '../../interfaces/IAuthor';

@Component({
    selector: 'app-banner-author',
    templateUrl: './banner-author.component.html',
    styleUrls: ['./banner-author.component.scss']
})
export class BannerAuthorComponent implements OnInit {

    @Input() author: IAuthor;

    constructor() {
    }

    ngOnInit(): void {
    }

}
