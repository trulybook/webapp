import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BannerAuthorComponent} from './banner-author.component';

describe('BannerAuthorComponent', () => {
    let component: BannerAuthorComponent;
    let fixture: ComponentFixture<BannerAuthorComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [BannerAuthorComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(BannerAuthorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
