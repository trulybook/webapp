import {Component, OnInit} from '@angular/core';
import {Utility} from '../../objects/Utility';
import {IMenu} from '../../interfaces/IMenu';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    navigationMenu: Array<IMenu> = Utility.getNavigationMenu();
    authMenu: Array<IMenu> = Utility.getAuthMenu();

    constructor() {
    }

    ngOnInit(): void {
    }

}
