import {Component, Input, OnInit} from '@angular/core';
import {IMenu} from '../../interfaces/IMenu';
import {CONSTANTS} from "../../../config/constants";
import {IConstants} from "../../interfaces/IConstants";

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

    CONSTANTS: IConstants = CONSTANTS;
    @Input() about$: Array<IMenu>;
    @Input() social$: Array<IMenu>;

    constructor() {
    }

    ngOnInit(): void {
    }

}
