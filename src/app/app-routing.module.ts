import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './pages/home/home.component';
import {AuthorComponent} from './pages/author/author.component';
import {AuthorBooksListComponent} from "./pages/author/author-books-list/author-books-list.component";
import {ContentBookListComponent} from "./pages/author/content-book-list/content-book-list.component";

const routes: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent},
    {
        path: 'author/:id', component: AuthorComponent,
        children: [
            {path: '', component: AuthorBooksListComponent},
            {path: ':type/:id', component: ContentBookListComponent}
        ]
    },
    {path: '**', redirectTo: 'home'}
];

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forRoot(routes, {useHash: true})
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
