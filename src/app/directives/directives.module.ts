import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TypeCardDirective} from './type-card/type-card.directive';

@NgModule({
    declarations: [
        TypeCardDirective
    ],
    exports: [
        TypeCardDirective
    ],
    imports: [
        CommonModule
    ]
})
export class DirectivesModule {
}
