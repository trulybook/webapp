import {Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
    selector: '[appTypeCard]'
})
export class TypeCardDirective implements OnInit {

    @Input() appTypeCard: any;
    private el: ElementRef;

    constructor(el: ElementRef) {
        this.el = el;
    }

    ngOnInit(): void {
        this.el.nativeElement.classList.add(this.appTypeCard);
    }

}
