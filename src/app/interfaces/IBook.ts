export interface IBook {
    id_book: number;
    title: string;
    description: string;
    img: string;
    id_category: number;
    category?: string;
    id_author: number;
    name?: string;
    surname?: string;
    motto?: string;
}
