import {IBook} from './IBook';

export interface ILibro extends IBook {
    id_content_book: number;
    content_book_filename: string;
}
