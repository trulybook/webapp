import {IBook} from './IBook';
import {ILibro} from './ILibro';
import {IVideo} from './IVideo';
import {IAudio} from './IAudio';

export interface IContents extends IBook, ILibro, IVideo, IAudio {
    book: IBook;
    libro?: Array<ILibro>;
    video?: Array<IVideo>;
    audio?: Array<IAudio>;
}
