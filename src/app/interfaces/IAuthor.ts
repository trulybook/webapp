export interface IAuthor {
    id_author: number;
    name: string;
    surname: string;
    motto: string;
    avatar: string;
}
