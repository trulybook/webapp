import {IBook} from './IBook';

export interface IAudio extends IBook {
    id_content_audio: number;
    content_audio_filename: string;
    content_audio_label: string;
}
