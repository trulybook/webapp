import {IBook} from './IBook';

export interface IVideo extends IBook {
    id_content_video: number;
    content_video_filename: string;
    content_video_label: string;
}
