import {IAuthor} from './IAuthor';
import {IBook} from './IBook';

export interface IAuthorDetails {
    author: IAuthor;
    books: Array<IBook>;
}
