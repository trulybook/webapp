export interface IConstants {
    BANNER: {
        MOTTO: string
    };
    HOME: {
        THE_STORIES: string,
        IN_EVIDENCE: string,
        VIDEO: string,
        AUDIO: string
    };
    CARD_HOME: {
        AUTHOR_OF: string,
        READ: string,
        SHOW_VIDEO: string,
        HEAR_AUDIO: string
    };
    CARD_EXTENDED: {
        READ: string,
        SHOW: string,
        HEAR: string
    };
    CARD_EXTENDED_SMALL: {
        READ: string,
        SHOW: string,
        HEAR: string
    };
    SIDEBAR: {
        ABOUT: string,
        SOCIAL: string
    };
    FOOTER: {
        RESUME: string,
        TAG: {
            CATEGORY: string
            BOOK: string,
            PUBLISHING: string,
            CREATIVE_STORIES: string,
            STORY_TELLING: string,
            JOBS: string
        },
        ABOUT: {
            CATEGORY: string,
            ABOUT_US: string,
            HOW_IT_WORKS: string,
            JOBS: string
        },
        SOCIAL: {
            CATEGORY: string,
            NEWSLETTER: string,
            EVENTS: string
        },
        COPYRIGHT: string
    };
    LABEL: {
        CLOSE: string
    };
}
