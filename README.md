# webapp

_**This README contains all the necessary information about front-end of TrulyBook.**_

**Scripts and informations**

| Command | Script |
| ------ | ------ |
| Start | npm start |

The back-end application does not respond to the _https_ protocol (in progress), so check that the call url is _http_.

**Environments**

Production: http://poprockserver.appspot.com/#/home

**How to release**

Before releasing make sure you don't have any code to push.

- After entering the code on the master branch, enter the _Google Cloud Platform_, open the console and run the
  following command: `git pull`
- When the pull of the master branch is completed, you need to build the webapp with the script: `npm run build`
- Finally, release the new application on the cloud with the command: `gcloud app deploy`
